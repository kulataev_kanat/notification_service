from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Сервис уведомлений",
        default_version='v1.0',
        description="Сервис который по заданным правилам запускает рассылку по списку клиентов.",
        license=openapi.License(name="Enot License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),

)

urlpatterns = [
    path('swagger(?P^<format>/.json|/.yaml$)/', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
