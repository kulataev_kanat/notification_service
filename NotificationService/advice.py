# exceptions
USER_DETAIL = "user"
ADMIN_DETAIL = "admin"
DATA_DETAIL = "detail"
ACCESS_TOKEN_DETAIL = "access_token"
USER_NOT_FOUND = "Пользователь не найден."
PAGE_NOT_FOUND = "Страница не найдена."
USER_BLOCKED = "Пользвователь заблокирован."
USER_NOT_ACTIVE = "Пользвователь не активен."
WRONG_PASSWORD = "Неверное имя пользователя или пароль."
AUTH_CREDENTIALS = "Учетные данные не были предоставлены."
ACCESS_TOKEN_EXPIRED = "Срок действия токена-доступа истек."
TOKEN_PREFIX = "Префикс токена отсутствует."
DATA_NOT_FOUND = "Данные не найдены."

# status
DELIVERED = "Доставлено"
IN_PROGRESS = "В Процессе"
CANCELED = "Отменено"
