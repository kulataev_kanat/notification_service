import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NotificationService.settings')

app = Celery('NotificationService')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'sending-messages': {
        'task': 'api.tasks.addition_data_scheduled_time',
        'schedule': crontab(minute='*/1'),
    },

}
