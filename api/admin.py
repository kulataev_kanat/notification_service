from django.contrib import admin

import api.models as model

admin.site.register(model.User)
admin.site.register(model.Distribution)
admin.site.register(model.Message)
