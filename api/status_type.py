from django.utils.translation import gettext_lazy as _

import NotificationService.advice as str_data

STATUS_TYPE = (
    (str_data.CANCELED, _(str_data.CANCELED)),
    (str_data.DELIVERED, _(str_data.DELIVERED)),
    (str_data.IN_PROGRESS, _(str_data.IN_PROGRESS)),
)
