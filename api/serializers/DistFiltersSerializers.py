from rest_framework import serializers

from api.models import Distribution, User
from api.serializers import MessageSerializers
from api.service import TimeWithTimezoneField


class GetUserDistSerializer(serializers.ModelSerializer):
    """Вывод и Поиск пользователей для рассылок"""

    groups = serializers.StringRelatedField()
    timezone = TimeWithTimezoneField()

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'phone',
            'operator_code',
            'tag',
            'timezone',
            'email',
            'password',
            'groups',
            'is_active',
        ]


class GetDistributionSerializer(serializers.ModelSerializer):
    """Вывод рассылок"""

    messages = MessageSerializers.AllMessageSerializer(many=True, allow_null=True)
    clients = GetUserDistSerializer(many=True, allow_null=True)

    class Meta:
        ref_name = "Get Distribution Serializer"
        model = Distribution
        fields = [
            'id',
            'start',
            'end',
            'messages',
            'clients',
        ]
