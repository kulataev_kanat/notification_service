from rest_framework import serializers

from api.models import Message


class AllMessageSerializer(serializers.ModelSerializer):
    """Добавление и Изменение сообщений"""

    class Meta:
        model = Message
        fields = [
            'status',
            'text',
        ]


class GetMessageSerializer(serializers.ModelSerializer):
    """Вывод сообщений"""

    class Meta:
        model = Message
        fields = '__all__'
