from rest_framework import serializers

from api.models import User
from api.serializers import DistributionSerializers
from api.service import TimeWithTimezoneField


class AuthSerializer(serializers.ModelSerializer):
    """Авторизация, вывод access токена"""

    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]


class CreateUserSerializer(serializers.ModelSerializer):
    """Добавление пользователя"""

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'phone',
            'operator_code',
            'email',
            'password',
            'tag',
            'groups',
            'timezone',
            'is_active',
        ]
        read_only_fields = [
            'is_active',
        ]


class UpdateRequestUserSerializer(serializers.ModelSerializer):
    """Изменение пользователя по идентификации"""

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'phone',
            'operator_code',
            'password',
            'tag',
            'timezone',
            'groups',
            'is_active',
        ]


class UpdateResponseUserSerializer(serializers.ModelSerializer):
    """Вывод измененного пользователя по идентификации"""

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'phone',
            'operator_code',
            'email',
            'tag',
            'password',
            'timezone',
            'groups',
            'is_active',
        ]


class GetUserSerializer(serializers.ModelSerializer):
    """Вывод и Поиск пользователей"""

    groups = serializers.StringRelatedField()
    timezone = TimeWithTimezoneField()
    distributions = DistributionSerializers.GetDistributionSerializer(many=True, source="client_id")

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'phone',
            'operator_code',
            'tag',
            'timezone',
            'email',
            'password',
            'groups',
            'is_active',
            'distributions',
        ]