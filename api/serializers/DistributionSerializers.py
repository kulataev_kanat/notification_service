from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from api.models import Distribution, User
from api.serializers import MessageSerializers
from api.service import AdditionSlugRelatedField


class AllDistributionSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    """Добавление, Изменение рассылок"""

    operator_code = AdditionSlugRelatedField(many=True,
                                             slug_field='operator_code',
                                             queryset=User.objects.all(),
                                             required=False,
                                             source='clients'
                                             )

    client_tag = AdditionSlugRelatedField(many=True,
                                          slug_field='tag',
                                          queryset=User.objects.all(),
                                          required=False,
                                          source='clients'
                                          )

    messages = MessageSerializers.AllMessageSerializer(many=True, allow_null=True)

    class Meta:
        model = Distribution
        fields = [
            'start',
            'end',
            'messages',
            'operator_code',
            'client_tag',
        ]


class GetDistributionSerializer(serializers.ModelSerializer):
    """Вывод рассылок"""

    messages = MessageSerializers.GetMessageSerializer(many=True, allow_null=True)

    class Meta:
        model = Distribution
        fields = [
            'id',
            'start',
            'end',
            'messages',
        ]
