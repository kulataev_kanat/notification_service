from django.urls import path
from rest_framework.decorators import permission_classes, authentication_classes
from rest_framework.permissions import AllowAny

from api.authentication import SafeJWTAuthentication
from api.permissions.permission import ROLE_ADMIN, ROLE_USER, UserPermissionsObj, AllowAnyGroups
from api.views import UserViews, GroupViews, DistributionViews, MessageViews, DistFiltersViews

allow_any = AllowAny
admin = ROLE_ADMIN
user = ROLE_USER
any_groups = AllowAnyGroups
own_object = UserPermissionsObj

urlpatterns = [
    # USERS
    path("user/access_token/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.AccessToken)).as_view()),
    path("user/create_user/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.CreateUserView)).as_view()),
    path("user/delete_user_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.DeleteUserByIdView)).as_view()),
    path("user/delete_all_users/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.DeleteAllUsersView)).as_view()),
    path("user/update_user_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.UpdateUserView)).as_view()),
    path("user/find_all_users/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.GetUserView)).as_view()),
    path("user/find_user_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(UserViews.FindUserByIdView)).as_view()),

    # GROUP
    path("group/create_group/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(GroupViews.CreateGroupView)).as_view()),
    path("group/delete_group_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(GroupViews.DeleteGroupByIdView)).as_view()),
    path("group/update_group_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(GroupViews.UpdateGroupByIdView)).as_view()),
    path("group/find_all_groups/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(GroupViews.GetGroupView)).as_view()),
    path("group/find_group_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(GroupViews.FindGroupByIdView)).as_view()),

    # DISTRIBUTION
    path("distribution/create_distribution/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.CreateDistributionView)).as_view()),
    path("distribution/delete_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.DeleteDistributionByIdView)).as_view()),
    path("distribution/delete_all_distributions/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.DeleteAllDistributionsView)).as_view()),
    path("distribution/update_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.UpdateDistributionByIdView)).as_view()),
    path("distribution/find_all_distributions/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.GetDistributionView)).as_view()),
    path("distribution/find_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistributionViews.FindDistributionByIdView)).as_view()),

    # DISTRIBUTION FILTERS
    path("dist_filters/find_all_canceled_distributions/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetAllCanceledDistributionsView)).as_view()),
    path("dist_filters/find_canceled_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetCanceledDistributionByIdView)).as_view()),
    path("dist_filters/find_all_progressed_distributions/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetAllProgressedDistributionsView)).as_view()),
    path("dist_filters/find_progressed_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetProgressedDistributionByIdView)).as_view()),
    path("dist_filters/find_all_delivered_distributions/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetAllDeliveredDistributionsView)).as_view()),
    path("dist_filters/find_delivered_distribution_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(DistFiltersViews.GetDeliveredDistributionByIdView)).as_view()),

    # MESSAGE
    path("message/create_message/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(MessageViews.CreateMessageView)).as_view()),
    path("message/delete_message_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(MessageViews.DeleteMessageByIdView)).as_view()),
    path("message/update_message_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(MessageViews.UpdateMessageByIdView)).as_view()),
    path("message/find_all_messages/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(MessageViews.GetMessageView)).as_view()),
    path("message/find_message_by_id/<int:pk>/",
         authentication_classes([SafeJWTAuthentication])(
             permission_classes([allow_any])(MessageViews.FindMessageByIdView)).as_view()),
]
