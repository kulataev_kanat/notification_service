from datetime import datetime

from django.contrib.auth.models import Group, AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from api.managers import UserAccountManager
from NotificationService.models import BaseModel
from django.utils import timezone

from api.status_type import STATUS_TYPE


def role_null():
    return Group.objects.get_or_create(name='role_null')[0]


class Distribution(BaseModel):
    start = models.DateTimeField(null=False, blank=True, default=datetime.now)
    end = models.DateTimeField(null=False, blank=True, default=datetime.now)

    class Meta:
        verbose_name = _('Рассылка')
        verbose_name_plural = _('Рассылки')


class User(BaseModel, AbstractUser):
    groups = models.ForeignKey(Group, on_delete=models.SET(role_null), null=True, blank=True)
    email = models.EmailField(max_length=50, unique=False, blank=True, null=True)
    phone = models.CharField(max_length=30, blank=True, default='7XXXXXXXXXX')
    operator_code = models.CharField(max_length=10, blank=True, default='+90')
    tag = models.CharField(unique=True, max_length=40, blank=True, default='@user_name')
    timezone = models.DateTimeField(blank=False, default=timezone.now)
    client_id = models.ManyToManyField(Distribution, blank=True, related_name='clients')

    objects = UserAccountManager()

    REQUIRED_FIELDS = ['groups_id', 'email']
    USERNAME_FIELD = 'username'
    REQUIRED_ADMIN_FIELDS = ['email']

    def __str__(self):
        return self.username.__str__()

    class Meta:
        db_table = 'api_user'
        verbose_name = _('Пользоваетель')
        verbose_name_plural = _('Пользователи')


class Message(BaseModel):
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS_TYPE, max_length=100, blank=True, null=True,
                              default=STATUS_TYPE[0][0])
    text = models.TextField(max_length=5000, blank=True, default="message_text")
    distribution_id = models.ManyToManyField(Distribution, blank=True,
                                             related_name='messages')

    class Meta:
        verbose_name = _('Сообщение')
        verbose_name_plural = _('Сообщения')
