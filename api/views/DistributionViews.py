from django.http import Http404
from rest_framework import generics, status
from rest_framework.response import Response

from NotificationService.advice import IN_PROGRESS
from api.models import Distribution
from api.serializers import DistributionSerializers


class CreateDistributionView(generics.CreateAPIView):
    """Добавление рассылки"""

    serializer_class = DistributionSerializers.AllDistributionSerializer

    def perform_create(self, serializer):
        serializer.save(
            messages={
                'status': IN_PROGRESS
            }
        )


class DeleteDistributionByIdView(generics.DestroyAPIView):
    """Удаление рассылки по идентификации"""

    queryset = Distribution.objects.all()


class DeleteAllDistributionsView(generics.DestroyAPIView):
    """Удаление всех рассылок"""

    def get_object(self):
        try:
            return Distribution.objects.all()
        except Distribution.DoesNotExist:
            raise Http404

    def delete(self, request, format=None, **kwargs):
        """Метод удаления всех рассылок"""
        distributions = self.get_object()
        distributions.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UpdateDistributionByIdView(generics.UpdateAPIView):
    """Обновление рассылки по идентификации"""

    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializers.AllDistributionSerializer

    def perform_update(self, serializer):
        serializer.save(
            messages={
                'status': IN_PROGRESS
            }
        )


class GetDistributionView(generics.ListAPIView):
    """Вывод рассылок"""

    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializers.GetDistributionSerializer


class FindDistributionByIdView(generics.RetrieveAPIView):
    """Поиск рассылки по идентификации"""

    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializers.GetDistributionSerializer
