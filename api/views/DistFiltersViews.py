from rest_framework import generics

from NotificationService.advice import CANCELED, IN_PROGRESS, DELIVERED
from api.models import Distribution
from api.serializers import DistFiltersSerializers


class GetAllCanceledDistributionsView(generics.ListAPIView):
    """Вывод рассылок с отмененными сообщениями"""

    queryset = Distribution.objects.filter(messages__status=CANCELED)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer


class GetCanceledDistributionByIdView(generics.RetrieveAPIView):
    """Вывод рассылок с отмененными сообщениями по идентификации"""

    queryset = Distribution.objects.filter(messages__status=CANCELED)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer


class GetAllProgressedDistributionsView(generics.ListAPIView):
    """Вывод рассылок с сообщениями которые в процессе"""

    queryset = Distribution.objects.filter(messages__status=IN_PROGRESS)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer


class GetProgressedDistributionByIdView(generics.RetrieveAPIView):
    """Вывод рассылок с сообщениями которые в процессе по идентификации"""

    queryset = Distribution.objects.filter(messages__status=IN_PROGRESS)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer


class GetAllDeliveredDistributionsView(generics.ListAPIView):
    """Вывод рассылок с доставленными сообщениями"""

    queryset = Distribution.objects.filter(messages__status=DELIVERED)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer


class GetDeliveredDistributionByIdView(generics.RetrieveAPIView):
    """Вывод рассылок с доставленными сообщениями по идентификации"""

    queryset = Distribution.objects.filter(messages__status=DELIVERED)
    serializer_class = DistFiltersSerializers.GetDistributionSerializer
