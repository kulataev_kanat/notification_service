from rest_framework import generics

from api.models import Message
from api.serializers import MessageSerializers


class CreateMessageView(generics.CreateAPIView):
    """Добавление сообщения"""

    serializer_class = MessageSerializers.AllMessageSerializer


class DeleteMessageByIdView(generics.DestroyAPIView):
    """Удаление сообщения по идентификации"""

    queryset = Message.objects.all()


class UpdateMessageByIdView(generics.UpdateAPIView):
    """Обновление сообщения по идентификации"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializers.AllMessageSerializer


class GetMessageView(generics.ListAPIView):
    """Вывод сообщений"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializers.GetMessageSerializer


class FindMessageByIdView(generics.RetrieveAPIView):
    """Поиск сообщения по идентификации"""

    queryset = Message.objects.all()
    serializer_class = MessageSerializers.GetMessageSerializer
