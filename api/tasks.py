from NotificationService.advice import DELIVERED, CANCELED
from django.utils import timezone

from NotificationService.celery import app
from api.models import Distribution


@app.task
def addition_data_scheduled_time():
    distributions = Distribution.objects.all()

    for distribution in distributions:
        if distribution.start <= timezone.now() <= distribution.end:
            for message in distribution.messages.all():
                message.status = DELIVERED
                message.save()

        if distribution.end <= timezone.now():
            for message in distribution.messages.all():
                message.status = CANCELED
                message.save()


addition_data_scheduled_time()
