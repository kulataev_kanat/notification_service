import datetime

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.fields import Field

from django.utils import timezone


class ChoiceField(serializers.ChoiceField, serializers.ReadOnlyField):
    """Сервис вывода данных choices"""

    def to_representation(self, obj):
        if obj == '' and self.allow_blank:
            return obj
        return self._choices[obj]

    def to_internal_value(self, data):
        if data == '' and self.allow_blank:
            return ''

        for key, val in self._choices.items():
            if val == data:
                return key
        self.fail('invalid_choice', input=data)


class PartialUpdateService(GenericAPIView):
    """Сервис обновления (patch)"""

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer, *args, **kwargs)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer, *args, **kwargs):
        serializer.save()


class PartialUpdateServiceView(PartialUpdateService):
    """Generic сервис обновления (patch)"""

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class AdditionSlugRelatedField(serializers.SlugRelatedField):
    """Сервис добавления ForeignKey поля"""

    def to_internal_value(self, data):
        try:
            return self.get_queryset().get(**{self.slug_field: data})
        except ObjectDoesNotExist:
            return Response("Data not found.")
        except (TypeError, ValueError):
            self.fail('invalid')


class CreatableSlugRelatedField(serializers.SlugRelatedField):
    """Сервис добавления и создания ForeignKey поля"""

    def to_internal_value(self, data):
        try:
            return self.get_queryset().get(**{self.slug_field: data})
        except ObjectDoesNotExist:
            return self.get_queryset().create(**{self.slug_field: data})
        except (TypeError, ValueError):
            self.fail('invalid')


class TimeWithTimezoneField(Field):
    default_error_messages = {
        'invalid': 'Time has wrong format, expecting %H:%M:%S%z.',
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def to_internal_value(self, value):
        value_with_date = datetime.datetime.now().strftime('%Y-%m-%d') + ' ' + value
        try:
            parsed = datetime.datetime.strptime(value_with_date, '%Y-%m-%d %H:%M:%S%z')
        except (ValueError, TypeError) as e:
            pass
        else:
            return parsed
        self.fail('invalid')

    def to_representation(self, value):
        if not value:
            return None

        if isinstance(value, str):
            return value

        return timezone.make_naive(value, timezone.utc).strftime("%H:%M:%S+00:00")
