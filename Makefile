makemigrate_api:
	python manage.py makemigrations api

migrate_api:
	python manage.py migrate api --database=default

migrate_django_tables:
	python manage.py migrate --database=default

celery_worker:
	celery -A NotificationService worker --loglevel=info --pool=solo

celery_beat:
	celery -A NotificationService beat -l info

docker_build:
	docker-compose up --build

docker_up:
	docker-compose up

docker_containers:
	docker-compose ps

run:
	python manage.py runserver

createsuperuser:
	python manage.py createsuperuser

install:
	pip install -r requirements.txt

freeze:
	pip freeze > requirements.txt