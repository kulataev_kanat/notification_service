import os
import sys

import django


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NotificationService.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


def bootstrap():
    """Bootstrap method"""

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NotificationService.settings')
    django.setup()

    from django.contrib.auth.models import Group

    admin = Group.objects.filter(name="admin").first()
    if not admin:
        group = Group()
        group.name = "admin"
        group.save()


if __name__.__eq__('__main__'):
    main()
    bootstrap()
